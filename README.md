# browserpage

A default page for your browser with bookmarks and search

## Run
Clone [this repository](https://codeberg.org/NoNamePro0/browserpage) and edit the config folder.

Then run this command to host it on port `8080`:
```bash
sudo docker run --rm -it -p 8080:80 -v "$PWD/config:/var/www/config:ro" browserpage
```