var engines = [];
var searchEngine;
var searchQueryVariable;

async function loadEngines(url, queryVariable) {
    searchQueryVariable = queryVariable

    const response  = await fetch(url);
    const csv       = await response.text();

    csv.split("\n").forEach(function(engineLine) {
        engines.push(engineLine.split(";"))
    });
}

async function setSearchEngine(name) {
    engines.forEach(function(engine) {
        if (engine[0] == name) {
            searchEngine = engine;
        }
    });
    console.log(searchEngine)
}

async function search(text) {
    window.open(searchEngine[1].replace(searchQueryVariable, text));
}
async function searchInputbox() {
    await search(document.getElementById('search').value);
}

document.getElementById("search").focus();