async function loadBookmarks(filename) {
    const json  = await fetch(filename);
    const groups = await json.json();

    const bookmarksRoot = document.getElementById("bookmarksRoot");
    bookmarksRoot.innerHTML = "";

    groups.forEach(group => {
        var element       = bookmarksRoot.appendChild(document.createElement("li"));
        element.className = "nav-item dropdown";
  
        var text          = element.appendChild(document.createElement("a"));
        text.className    = "nav-link dropdown-toggle"
        text.href         = "#";
        text.id           = group.id;
        text.innerHTML    = group.name;
        text.setAttribute("role", "button");
        text.setAttribute("data-toggle", "dropdown");
        text.setAttribute("aria-haspopup", "true");
        text.setAttribute("aria-expanded", "false");

        var items	 = element.appendChild(document.createElement("div"));
        items.className  = "dropdown-menu";
        items.setAttribute("aria-labelledby", "navbarDropdown");	

       group.bookmarks.forEach(bookmark => {
            var bookmarkElement		= items.appendChild(document.createElement("a"));
            bookmarkElement.id		= bookmark.id;
            bookmarkElement.className	= "dropdown-item";
            bookmarkElement.setAttribute("href", bookmark.url);
            bookmarkElement.setAttribute("target", "_blank");
            bookmarkElement.setAttribute("rel", "noopener noreferrer");
            bookmarkElement.innerHTML	= bookmark.name;
        });
    });
}